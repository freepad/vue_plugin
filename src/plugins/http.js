import Vue from 'vue'
import store from '../store'

class Http {
  static install (Vue) {
    Vue.prototype.$http = (options) => {
      return store.state.token || 'token_not_found'
    }
  }
}

Vue.use(Http)

export default Http
