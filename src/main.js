import Vue from 'vue'
import App from './App.vue'
import store from './store'
import http from './plugins/http'

Vue.config.productionTip = false

new Vue({
  store,
  http,
  render: h => h(App)
}).$mount('#app')
