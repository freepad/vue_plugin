import Vue from 'vue'
import Vuex from 'vuex'
import Http from 'http'

Vue.use(Vuex)
Vue.use(Http)

const state = {
  token: null
}

const mutations = {
  setToken (state, payload) {
    state.token = payload
  }
}

const actions = {
  fetchToken ({ commit }) {
    const token = new Vue().$http()
    commit('setToken', token)
  }
}

export default new Vuex.Store({
  state,
  mutations,
  actions
})
